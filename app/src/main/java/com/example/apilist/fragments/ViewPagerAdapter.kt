package com.example.apilist.fragments

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.apilist.api
import com.example.apilist.databinding.FragmentPokemonDetailBinding

class ViewPagerAdapter:
    RecyclerView.Adapter<ViewPagerAdapter.ViewPagerHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewPagerHolder {
        val binding = FragmentPokemonDetailBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewPagerHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewPagerHolder, position: Int) {
        holder.bind(position + 1)
    }

    override fun getItemCount(): Int {
        return api.pokemonGenSize
    }

    class ViewPagerHolder(private val binding: FragmentPokemonDetailBinding) :
        RecyclerView.ViewHolder(binding.root) {
            fun bind(currentId: Int) {
                PokemonDetailAdapter(binding, currentId)
            }
        }
}
