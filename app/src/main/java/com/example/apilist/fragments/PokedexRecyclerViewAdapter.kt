package com.example.apilist.fragments

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.apilist.PokemonTreatment.Status
import com.example.apilist.PokemonTreatment.capitalize
import com.example.apilist.PokemonTreatment.setOnCLickToFavorites
import com.example.apilist.PokemonTreatment.toStringId
import com.example.apilist.PokemonTreatment.trimBorders
import com.example.apilist.databinding.FragmentPokemonBinding
import com.example.apilist.model.PokemonContent

class PokedexRecyclerViewAdapter(
    private val values: MutableList<PokemonContent>
) : RecyclerView.Adapter<PokedexRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            FragmentPokemonBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.idView.text = item.pokemon.id.toStringId()
        holder.contentView.text = item.pokemon.name.capitalize()
        holder.imageView.setImageBitmap(item.image.default.trimBorders(0))

        holder.favourite.setOnCLickToFavorites()

        setOnClickToItem((holder.itemView as LinearLayout), item.pokemon.id)
    }

    private fun setOnClickToItem(itemLayout: LinearLayout, id: Int) {
        itemLayout.setOnClickListener {
            val direction = PokedexFragmentDirections.actionPokedexFragmentToItemHolderFragment(id)
            itemLayout.findNavController().navigate(direction)
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(binding: FragmentPokemonBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val idView: TextView = binding.itemNumber
        val contentView: TextView = binding.content
        val imageView: ImageView = binding.iconView
        val favourite: ImageButton = binding.favorite
    }
}
