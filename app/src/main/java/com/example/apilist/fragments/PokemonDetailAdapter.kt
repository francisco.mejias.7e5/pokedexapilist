package com.example.apilist.fragments

import android.content.res.ColorStateList
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.apilist.PokemonTreatment.Colors
import com.example.apilist.PokemonTreatment.Status
import com.example.apilist.PokemonTreatment.capitalize
import com.example.apilist.PokemonTreatment.filterDescription
import com.example.apilist.PokemonTreatment.opposite
import com.example.apilist.PokemonTreatment.setOnCLickToFavorites
import com.example.apilist.PokemonTreatment.toHeightString
import com.example.apilist.PokemonTreatment.toStringId
import com.example.apilist.PokemonTreatment.toWeightString
import com.example.apilist.PokemonTreatment.trimBorders
import com.example.apilist.R
import com.example.apilist.api
import com.example.apilist.databinding.FragmentPokemonDetailBinding
import com.example.apilist.extPokemonAndImages
import com.example.apilist.extPokemonSpecies
import com.example.apilist.model.PokemonContent
import com.example.apilist.modelSerializable.pokemonSpecies.PokemonSpecies
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking


class PokemonDetailAdapter(binding: FragmentPokemonDetailBinding, currentId: Int) {
    init {
        val pokemonContent = extPokemonAndImages.find { it.pokemon.id == currentId }!!

        changeStyleToPokemon(pokemonContent, binding)
    }

    private fun changeStyleToPokemon(
        pokemonContent: PokemonContent,
        binding: FragmentPokemonDetailBinding
    ) {
        runBlocking {
            var pokemonDetail: PokemonSpecies? = extPokemonSpecies[pokemonContent.pokemon.id]
            val job = coroutineScope { launch {
                if (pokemonDetail == null) {
                    pokemonDetail = api.pokemonSpecies(pokemonContent.pokemon.id)
                    extPokemonSpecies[pokemonContent.pokemon.id] = pokemonDetail
                }
            } }

            binding.name.text = pokemonContent.pokemon.name.capitalize()
            binding.id.text = pokemonContent.pokemon.id.toStringId()
            binding.favoriteDetail.setOnCLickToFavorites()
            binding.pokemonImage.setImageBitmap(pokemonContent.image.default.trimBorders(0))
            binding.weightText.text = pokemonContent.pokemon.weight.toWeightString()
            binding.heightText.text = pokemonContent.pokemon.height.toHeightString()

            with(binding.typesList) {
                val linearLayoutManager = LinearLayoutManager(context)
                linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
                layoutManager = linearLayoutManager
                adapter = TypesRecyclerViewAdapter(pokemonContent.pokemon.types)
            }

            binding.pokemonImage.tag = Status.INACTIVE

            binding.shinyButton.setOnClickListener {
                if (binding.pokemonImage.tag == Status.INACTIVE) {
                    binding.pokemonImage.setImageBitmap(pokemonContent.image.shiny.trimBorders(0))
                    binding.pokemonImage.tag = Status.ACTIVE
                } else {
                    binding.pokemonImage.setImageBitmap(pokemonContent.image.default.trimBorders(0))
                    binding.pokemonImage.tag = Status.INACTIVE
                }
            }

            job.join()

            val flavorText = pokemonDetail!!.flavor_text_entries.find { it.language.name == "en" }!!.flavor_text
            val filtered = flavorText.filterDescription()
            val genus = pokemonDetail!!.genera.find { it.language.name == "en" }!!.genus
            val description = "Description $genus\n\n$filtered"

            val color = Colors.valueOf(pokemonDetail!!.color.name.uppercase())
            val colorOpposite = color.opposite()

            binding.description.text = description
            binding.list.setBackgroundColor(color.value)

            binding.name.setTextColor(colorOpposite)
            binding.id.setTextColor(colorOpposite)
            binding.shinyButton.imageTintList = ColorStateList.valueOf(colorOpposite)
            if (colorOpposite == -1) {
                binding.shinyButton.setBackgroundResource(R.drawable.border_white)
            }
            binding.weightImage.imageTintList = ColorStateList.valueOf(color.value)
            binding.heightImage.imageTintList = ColorStateList.valueOf(color.value)
        }
    }
}