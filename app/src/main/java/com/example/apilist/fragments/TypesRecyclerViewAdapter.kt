package com.example.apilist.fragments

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toDrawable
import androidx.core.graphics.green
import androidx.core.graphics.toColor
import androidx.recyclerview.widget.RecyclerView
import com.example.apilist.MainActivity
import com.example.apilist.PokemonTreatment.capitalize
import com.example.apilist.R
import com.example.apilist.databinding.FragmentTypeBinding
import com.example.apilist.modelSerializable.pokemon.Type

class TypesRecyclerViewAdapter(
    private val types: List<Type>
) : RecyclerView.Adapter<TypesRecyclerViewAdapter.ViewHolder>() {
    enum class Icons(val drawable: Int, val color: Int) {
        BUG(R.drawable.type_bug, Color.parseColor("#84C404")),
        DRAGON(R.drawable.type_dragon, Color.parseColor("#0970BF")),
        ELECTRIC(R.drawable.type_electric, Color.parseColor("#FBD100")),
        FAIRY(R.drawable.type_fairy, Color.parseColor("#FB89EB")),
        FIGHTING(R.drawable.type_fighting, Color.parseColor("#E0306A")),
        FIRE(R.drawable.type_fire, Color.parseColor("#FB9443")),
        FLYING(R.drawable.type_flying, Color.parseColor("#8BABE3")),
        GHOST(R.drawable.type_ghost, Color.parseColor("#4C6BB4")),
        GRASS(R.drawable.type_grass, Color.parseColor("#3CBB4C")),
        GROUND(R.drawable.type_ground, Color.parseColor("#E87236")),
        ICE(R.drawable.type_ice, Color.parseColor("#4CD1C0")),
        NORMAL(R.drawable.type_normal, Color.parseColor("#919AA2")),
        POISON(R.drawable.type_poison, Color.parseColor("#B567CE")),
        PSYCHIC(R.drawable.type_psychic, Color.parseColor("#FF6675")),
        ROCK(R.drawable.type_rock, Color.parseColor("#C8B686")),
        STEEL(R.drawable.type_steel, Color.parseColor("#5A8EA2")),
        WATER(R.drawable.type_water, Color.parseColor("#3692DC"))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            FragmentTypeBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = types[position]
        val icon = Icons.valueOf(item.values.name.uppercase())
        holder.itemView.backgroundTintList = ColorStateList.valueOf(icon.color)
        holder.icon.setImageResource(icon.drawable)
        holder.name.text = item.values.name.capitalize()
    }

    override fun getItemCount(): Int = types.size

    inner class ViewHolder(binding: FragmentTypeBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val icon: ImageView = binding.typeIcon
        val name: TextView = binding.typeName
    }
}