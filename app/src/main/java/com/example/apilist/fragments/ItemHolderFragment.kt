package com.example.apilist.fragments

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import com.example.apilist.databinding.FragmentItemHolderBinding


class ItemHolderFragment : Fragment() {
    private lateinit var binding: FragmentItemHolderBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentItemHolderBinding.inflate(layoutInflater)
        val args = ItemHolderFragmentArgs.fromBundle(requireArguments())

        binding.root.adapter = ViewPagerAdapter()

        binding.root.setCurrentItem(args.id -1, false)

        return binding.root
    }
}