package com.example.apilist.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.apilist.databinding.FragmentPokedexBinding
import com.example.apilist.extPokemonAndImages

class PokedexFragment : Fragment() {
    lateinit var binding: FragmentPokedexBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPokedexBinding.inflate(layoutInflater)

        with(binding.root) {
            layoutManager = LinearLayoutManager(context)
            adapter = PokedexRecyclerViewAdapter(extPokemonAndImages)
        }

        return binding.list
    }
}