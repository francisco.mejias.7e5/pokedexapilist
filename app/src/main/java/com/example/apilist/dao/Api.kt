package com.example.apilist.dao

import com.example.apilist.modelSerializable.evolutionChain.Chain
import com.example.apilist.modelSerializable.evolutionChain.Evolution
import com.example.apilist.modelSerializable.pokemon.Pokemon
import com.example.apilist.modelSerializable.pokemonSpecies.PokemonSpecies
import com.example.apilist.modelSerializable.pokemons.PokemonList
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json

class Api {
    private val client = HttpClient(OkHttp) {
        install(ContentNegotiation) {
            json(Json{
                ignoreUnknownKeys = true
            })
        }
    }

    val pokemonGenSize = 151

    private val pokeAPIURL = "https://pokeapi.co/api/v2"

    suspend fun pokemonList(): PokemonList = client.get("$pokeAPIURL/pokemon?limit=$pokemonGenSize").body()
    suspend fun pokemonByUrl(url: String): Pokemon = client.get(url).body()
    suspend fun pokemonSpecies(id: Int): PokemonSpecies = client.get("$pokeAPIURL/pokemon-species/$id").body()
    suspend fun pokemonEvolutionByUrl(url: String): Evolution = client.get(url).body()

    private fun Chain.printEvolutions() {
        println(this.species.name)
        this.evolves_to.forEach {
            it.printEvolutions()
        }
    }
}