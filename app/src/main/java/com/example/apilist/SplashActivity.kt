package com.example.apilist

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.animation.AnimationUtils
import com.example.apilist.databinding.SplashScreenBinding
import kotlinx.coroutines.*

@SuppressLint("CustomSplashScreen")
open class SplashActivity: ItemsActivity() {
    private lateinit var binding: SplashScreenBinding

    @SuppressLint("SetTextI18n")
    @OptIn(DelicateCoroutinesApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = SplashScreenBinding.inflate(layoutInflater)

        GlobalScope.launch {
            launch {
                 do {
                    delay(500)
                    val progress = (extPokemonAndImages.size * 100) / api.pokemonGenSize
                    binding.progressBar.progress = progress
                    binding.percentageText.text = "$progress%"
                } while (progress < 100)
            }
            val rotation = AnimationUtils.loadAnimation(binding.root.context, R.anim.rotate)
            rotation.fillAfter = true
            binding.logoRing.startAnimation(rotation)
            job.join()
            runOnUiThread {
                val intent = Intent(this@SplashActivity, MainActivity::class.java)
                startActivity(intent)
            }
        }

        setContentView(binding.root)
    }
}