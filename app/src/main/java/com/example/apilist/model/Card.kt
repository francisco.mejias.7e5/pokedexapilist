package com.example.apilist.model

import android.graphics.Bitmap
import android.widget.ImageButton
import com.example.apilist.modelSerializable.pokemon.Pokemon

class Card(
    val isShiny: Boolean,
    val pokemon: Pokemon,
    val image: Bitmap,
    val button: ImageButton,
    var flipped: Boolean
)
