package com.example.apilist.model

import com.example.apilist.modelSerializable.pokemon.Pokemon

class PokemonContent(
    val pokemon: Pokemon,
    val image: Image
)