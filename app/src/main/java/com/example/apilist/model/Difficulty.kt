package com.example.apilist.model

@kotlinx.serialization.Serializable
enum class Difficulty{
    EASY,
    MEDIUM,
    POKEMON_TRAINER
}