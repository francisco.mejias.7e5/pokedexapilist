package com.example.apilist.model

import android.graphics.Bitmap

class Image(
    val default: Bitmap,
    val shiny: Bitmap
)
