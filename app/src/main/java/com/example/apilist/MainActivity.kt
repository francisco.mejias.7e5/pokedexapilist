package com.example.apilist

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.example.apilist.databinding.MainScreenBinding
import com.example.apilist.databinding.SplashScreenBinding
import com.example.apilist.model.PokemonContent
import com.example.apilist.modelSerializable.pokemon.Pokemon
import kotlinx.coroutines.*
import java.lang.Exception
import kotlin.io.path.Path
import kotlin.io.path.appendText
import kotlin.io.path.createFile
import kotlin.io.path.readText

class MainActivity: AppCompatActivity() {
    private lateinit var binding: MainScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainScreenBinding.inflate(layoutInflater)

        setContentView(binding.root)
    }
}