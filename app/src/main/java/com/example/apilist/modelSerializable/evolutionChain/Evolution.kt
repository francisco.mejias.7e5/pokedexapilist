package com.example.apilist.modelSerializable.evolutionChain

import kotlinx.serialization.Serializable

@Serializable
data class Evolution(
    val chain: Chain
)