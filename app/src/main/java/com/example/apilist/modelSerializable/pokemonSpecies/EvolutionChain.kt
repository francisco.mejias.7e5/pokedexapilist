package com.example.apilist.modelSerializable.pokemonSpecies

import kotlinx.serialization.Serializable

@Serializable
data class EvolutionChain(
    val url: String
)