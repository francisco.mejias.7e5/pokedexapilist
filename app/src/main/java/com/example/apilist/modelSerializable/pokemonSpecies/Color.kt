package com.example.apilist.modelSerializable.pokemonSpecies

import kotlinx.serialization.Serializable

@Serializable
data class Color(
    val name: String
)