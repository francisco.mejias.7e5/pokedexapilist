package com.example.apilist.modelSerializable.pokemonSpecies

import kotlinx.serialization.Serializable

@Serializable
data class Genera(
    val genus: String,
    val language: Language
)