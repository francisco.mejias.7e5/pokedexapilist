package com.example.apilist.modelSerializable.pokemons

import kotlinx.serialization.Serializable

@Serializable
data class PokemonList(
    val results: List<Result>
)