package com.example.apilist.modelSerializable.pokemonSpecies

import kotlinx.serialization.Serializable

@Serializable
data class FlavorTextEntry(
    val flavor_text: String,
    val language: Language
)