package com.example.apilist.modelSerializable.pokemonSpecies

import kotlinx.serialization.Serializable

@Serializable
data class PokemonSpecies(
    val id: Int,
    val name: String,
    val color: Color,
    val flavor_text_entries: List<FlavorTextEntry>,
    val evolution_chain: EvolutionChain,
    val genera: List<Genera>
)