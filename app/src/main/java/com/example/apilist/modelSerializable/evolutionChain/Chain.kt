package com.example.apilist.modelSerializable.evolutionChain

import kotlinx.serialization.Serializable

@Serializable
data class Chain(
    val evolves_to: List<Chain>,
    val species: Species
)