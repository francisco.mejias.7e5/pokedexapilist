package com.example.apilist.modelSerializable.pokemon

import com.example.apilist.modelSerializable.pokemon.Ability
import kotlinx.serialization.Serializable

@Serializable
data class Pokemon(
    val id: Int,
    val name: String,
    val weight: Int,
    val height: Int,
    val types: List<Type>,
    val abilities: List<Ability>,
    val sprites: Sprites
)