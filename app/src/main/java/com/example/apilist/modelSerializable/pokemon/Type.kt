package com.example.apilist.modelSerializable.pokemon

import kotlinx.serialization.Serializable
import kotlinx.serialization.SerialName

@Serializable
data class Type(
    @SerialName("type") val values: Values
) {
    @Serializable
    data class Values(
        val name: String
    )
}