package com.example.apilist

import androidx.appcompat.app.AppCompatActivity
import com.example.apilist.dao.Api
import com.example.apilist.model.Image
import com.example.apilist.model.PokemonContent
import com.example.apilist.modelSerializable.pokemon.Pokemon
import com.example.apilist.modelSerializable.pokemonSpecies.PokemonSpecies
import com.example.apilist.modelSerializable.pokemons.Result
import com.squareup.picasso.Picasso
import kotlinx.coroutines.*
import java.lang.Exception

var extResultList = mutableListOf<Result>()
val extPokemonAndImages: MutableList<PokemonContent> = mutableListOf()
val extPokemonSpecies: MutableMap<Int, PokemonSpecies?> = mutableMapOf()
val api = Api()

@OptIn(DelicateCoroutinesApi::class)
abstract class ItemsActivity: AppCompatActivity() {
    var job: Job
    init {
        job = GlobalScope.launch {
            extResultList = api.pokemonList().results.toMutableList()
            extResultList.forEach {
                withTimeout(20000) {
                    var pokemon: Pokemon? = null
                    var loopCount = 1
                    while (pokemon == null) {
                        try {
                            pokemon = api.pokemonByUrl(it.url)
                        } catch (e: Exception) {
                            println("Intento $loopCount. Falla ${it.name} - Content")
                            loopCount++
                        }
                    }
                    val image = getPokemonImage(pokemon)
                    extPokemonAndImages += PokemonContent(pokemon, image)
                }
            }
            extPokemonAndImages.sortBy { it.pokemon.id }
            println("############################################################")
            println("Finished")
        }
        job.invokeOnCompletion {
            GlobalScope.launch {
                extPokemonAndImages.forEach { extPokemonSpecies[it.pokemon.id] = null }
                extPokemonAndImages.forEach {
                    withTimeout(20000) {
                        val pokemonId = it.pokemon.id
                        if (extPokemonSpecies[pokemonId] == null) {
                            var pokemonSpecies: PokemonSpecies? = null
                            var loopCount = 1
                            while (pokemonSpecies == null) {
                                try {
                                    pokemonSpecies = api.pokemonSpecies(pokemonId)
                                } catch (e: Exception) {
                                    println("Intento $loopCount. Falla ${it.pokemon.name} - Specie")
                                    loopCount++
                                }
                            }
                            extPokemonSpecies[pokemonId] = pokemonSpecies
                        }
                    }
                }
            }
        }
    }

    private fun getPokemonImage(pokemon: Pokemon): Image =
        Image(
            Picasso.get().load(pokemon.sprites.front_default).get(),
            Picasso.get().load(pokemon.sprites.front_shiny).get()
        )
}
