package com.example.apilist

import android.graphics.Bitmap
import android.graphics.Color
import android.widget.ImageButton
import androidx.core.graphics.blue
import androidx.core.graphics.green
import androidx.core.graphics.red
import androidx.core.view.isVisible
import com.google.android.material.snackbar.Snackbar

object PokemonTreatment {
    fun ImageButton.setOnCLickToFavorites() {
        this.tag = Status.INACTIVE
        this.setOnClickListener {
            val text: String
            if (this.tag == Status.INACTIVE) {
                this.setImageResource(R.drawable.pokeball_active)
                this.tag = Status.ACTIVE
                text = "Add to favorites"
            } else {
                this.setImageResource(R.drawable.pokeball_inactive)
                this.tag = Status.INACTIVE
                text = "Removed from favorites"
            }
            val snackbar = Snackbar.make(this, text, 3000)
                .setTextColor(Color.BLACK)

            snackbar.view.setBackgroundColor(Color.WHITE)
            snackbar.view.setOnClickListener {
                it.isVisible = false
            }
            snackbar.show()
        }
    }

    fun Bitmap.trimBorders(color: Int): Bitmap {
        var startX = 0
        loop@ for (x in 0 until width) {
            for (y in 0 until height) {
                if (getPixel(x, y) != color) {
                    startX = x
                    break@loop
                }
            }
        }
        var startY = 0
        loop@ for (y in 0 until height) {
            for (x in 0 until width) {
                if (getPixel(x, y) != color) {
                    startY = y
                    break@loop
                }
            }
        }
        var endX = width - 1
        loop@ for (x in endX downTo 0) {
            for (y in 0 until height) {
                if (getPixel(x, y) != color) {
                    endX = x
                    break@loop
                }
            }
        }
        var endY = height - 1
        loop@ for (y in endY downTo 0) {
            for (x in 0 until width) {
                if (getPixel(x, y) != color) {
                    endY = y
                    break@loop
                }
            }
        }

        val newWidth = endX - startX + 1
        val newHeight = endY - startY + 1

        return Bitmap.createBitmap(this, startX, startY, newWidth, newHeight)
    }

    fun String.capitalize() =
        this.first().uppercase() + this.substring(1).lowercase()

    enum class Status {
        INACTIVE,
        ACTIVE
    }

    fun Int.toStringId(): String = "#" + this.toString().padStart(3, '0')
    fun Int.toWeightString(): String = (this.toDouble() / 10.0).toString() + " kg"
    fun Int.toHeightString(): String = (this.toDouble() / 10.0).toString() + " m"

    fun String.filterDescription() =
        this.replace("POKéMON", "pokemon").replace("\n", " ")

    enum class Colors(val value: Int) {
        GREEN(Color.parseColor("#40b868")),
        RED(Color.parseColor("#f05868")),
        BLUE(Color.parseColor("#3088f0")),
        WHITE(Color.parseColor("#f0f0f0")),
        BROWN(Color.parseColor("#b07030")),
        YELLOW(Color.parseColor("#f0d048")),
        PURPLE(Color.parseColor("#a868c0")),
        PINK(Color.parseColor("#f890c8")),
        GRAY(Color.parseColor("#a0a0a0")),
        BLACK(Color.parseColor("#585858"))
    }

    fun Colors.opposite() =
        Color.parseColor(if((this.value.red*0.299 + this.value.green*0.587 + this.value.blue*0.114) > 186) { "#000000" } else { "#FFFFFF" })
}